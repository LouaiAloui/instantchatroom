import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga'

import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import reducers from './reducers';
import setupSocket from './sockets'
import handlNewMessage from './sagas'
import username from './utils/name'

const sagaMiddlleware = createSagaMiddleware()

const store = createStore(
  reducers,
  applyMiddleware(sagaMiddlleware)
);

const socket = setupSocket(store.dispatch, username)

sagaMiddlleware.run(handlNewMessage, {socket, username})

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

reportWebVitals();
